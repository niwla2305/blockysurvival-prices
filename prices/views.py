from django.shortcuts import render
from django.http import HttpResponse
from .import calculate


itemlist = []

itemlist.insert(0,
    {
        'description': 'Special Item',
        'itemstring': 'default:special',
        'amount': '1',
        'price': '1000'
    }
)



def home(request):
    context = {
        'itemlist': itemlist
    }
    return render(request, 'prices/home.html', context)

def edit(request):
    return render(request, 'prices/edit.html')

