from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='prices-home'),
    path('', views.home, name='prices-about'),
    path('edit/', views.edit, name='prices-edit')
]
